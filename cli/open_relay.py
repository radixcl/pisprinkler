#!/usr/bin/env python3
#encoding: utf-8

import sys
import os

os.chdir(os.path.dirname(__file__))
sys.path.append("..")

from lib import lib
import argparse
import time

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--gpio', nargs=1, required=True, action='append', type=str, help="GPIO number, can be a single one or multiple ones, separeted by :")
    parser.add_argument('-s', '--seconds', nargs=1, required=True, action='append', type=int, help="Seconds to open the relay")
    parser.add_argument('-b', '--beeps', nargs=1, required=False, type=int, help="Number of beeps before start")
    args = parser.parse_args()

    if len(args.gpio) != len(args.seconds):
        print("You must define duration for each gpio.")
        sys.exit(1)


    print("Starting...")    
    if args.beeps is not None:
        for x in range(0, args.beeps[0]):
            lib.beep(1)
            time.sleep(1)

    for idx, r in enumerate(args.gpio):
        print("relay:", r[0], idx, args.seconds[idx][0])
        lib.open_relay(r[0], args.seconds[idx][0])
