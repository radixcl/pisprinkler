#!/usr/bin/env python3
#encoding: utf-8

from . import db
from gpiozero import TonalBuzzer
from gpiozero import OutputDevice
from gpiozero.tones import Tone
import time

def get_config(key):
    db.c.execute("SELECT v FROM config WHERE k = ?", (key,))
    ret = db.c.fetchone()
    if ret is None:
        return None
    else:
        return ret[0]

def beep(seconds):
    gpio = int(get_config('buzzer_gpio'))
    tone = get_config('buzzer_tone')
    
    b = TonalBuzzer(gpio)
    b.play(tone)
    time.sleep(seconds)
    b.stop()

def open_relay(ids, seconds):

    relay = {}
    for id in ids.split(':'):
        print("open_relay start:", id, seconds)
        relay[id] = OutputDevice(id, active_high=False, initial_value=False)
        relay[id].on()

    time.sleep(seconds)
    for id in ids.split(':'):
        relay[id].off()
        print("open_relay end:", id, seconds)
