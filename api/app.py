from fastapi import FastAPI, HTTPException, Header
import os
import time
import uuid
from typing import List, Dict
from lib import lib

API_KEY = lib.get_config('api_key')
app = FastAPI()

# Diccionario para almacenar las tareas en ejecución
tasks: Dict[str, int] = {}  # task_id -> PID

# Funciones auxiliares para manejar los relays
def execute_task(gpios: List[str], seconds: List[int], beeps: int = None):
    """
    Función que ejecuta la lógica de control de los relays.
    """
    if beeps:
        for _ in range(beeps):
            lib.beep(1)
            time.sleep(1)

    for gpio, sec in zip(gpios, seconds):
        lib.open_relay(gpio, sec)
        print(f"Relay {gpio} abierto por {sec} segundos.")

# Endpoint para controlar los relays
@app.post("/control-relay")
async def control_relay(
    gpios: List[str],
    seconds: List[int],
    beeps: int = None,
    x_api_key: str = Header(None)
):
    """
    Endpoint para iniciar el control de relays.
    """
    # Verificar API key
    if x_api_key != API_KEY:
        raise HTTPException(status_code=401, detail="Invalid API Key")
    
    # Validar parámetros
    if len(gpios) != len(seconds):
        raise HTTPException(status_code=400, detail="Each GPIO must have a corresponding duration")
    
    # Generar un ID único para la tarea
    task_id = str(uuid.uuid4())
    pid = os.fork()  # Crear un proceso hijo

    if pid == 0:  # Proceso hijo
        try:
            execute_task(gpios, seconds, beeps)
        finally:
            os._exit(0)  # Asegurarse de salir correctamente del proceso hijo

    # Proceso padre almacena el PID de la tarea
    tasks[task_id] = pid
    return {"status": "Processing", "task_id": task_id, "gpios": gpios, "seconds": seconds, "beeps": beeps}

# Endpoint para consultar el estado de las tareas
@app.get("/tasks")
async def list_tasks(x_api_key: str = Header(None)):
    """
    Endpoint para listar las tareas activas.
    """
    # Verificar API key
    if x_api_key != API_KEY:
        raise HTTPException(status_code=401, detail="Invalid API Key")
    
    # Verificar si las tareas siguen activas
    active_tasks = {task_id: pid for task_id, pid in tasks.items() if os.waitpid(pid, os.WNOHANG)[0] == 0}
    return {"tasks": active_tasks}

# Endpoint para detener una tarea específica
@app.delete("/tasks/{task_id}")
async def stop_task(task_id: str, x_api_key: str = Header(None)):
    """
    Endpoint para detener una tarea específica.
    """
    # Verificar API key
    if x_api_key != API_KEY:
        raise HTTPException(status_code=401, detail="Invalid API Key")
    
    # Verificar si la tarea existe
    if task_id not in tasks:
        raise HTTPException(status_code=404, detail="Task not found")
    
    # Detener el proceso asociado a la tarea
    pid = tasks[task_id]
    try:
        os.kill(pid, 9)  # Forzar la terminación del proceso
        del tasks[task_id]  # Eliminar la tarea del registro
        return {"status": "Task terminated", "task_id": task_id}
    except ProcessLookupError:
        del tasks[task_id]
        raise HTTPException(status_code=404, detail="Task not found or already completed")

# Endpoint para detener todas las tareas en ejecución
@app.delete("/tasks")
async def stop_all_tasks(x_api_key: str = Header(None)):
    """
    Endpoint para detener todas las tareas activas.
    """
    # Verificar API key
    if x_api_key != API_KEY:
        raise HTTPException(status_code=401, detail="Invalid API Key")
    
    if not tasks:
        return {"status": "No tasks to terminate"}

    # Iterar sobre todas las tareas y detenerlas
    terminated_tasks = []
    for task_id, pid in list(tasks.items()):  # Usar list() para evitar problemas de iteración al modificar el dict
        try:
            os.kill(pid, 9)  # Forzar la terminación del proceso
            terminated_tasks.append(task_id)
            del tasks[task_id]  # Eliminar del registro
        except ProcessLookupError:
            del tasks[task_id]  # Si el proceso ya terminó, eliminar del registro

    return {"status": "All tasks terminated", "terminated_tasks": terminated_tasks}
